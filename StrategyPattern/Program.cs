﻿using StrategyPattern;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Strategy();
            Console.WriteLine();
            Visiter();
            
            Console.ReadKey();
        }

        private static void Visiter()
        {
            Shape circle = new Circle(20, 32, 10);
            Shape rectangle = new Rectangle(20, 32, 10, 10);
            Shape square = new Square(20, 32, 10);

            IVisitor visiter = new VisitorJson();

            string data = circle.Export(visiter);


            Console.WriteLine(data);
        }

        private static void Strategy()
        {
            Shape circle = new Circle(20, 32, 10);
            Rectangle rectangle = new Rectangle(20, 32, 10, 10);
            Shape square = new Square(20, 32,10);

            IExport exportxml = new ExportXml();
            IExport exportjson = new ExportJson();

            Context context = new Context(exportjson);

            string data = context.DoExport(circle);

            Console.WriteLine(data);
        }
    }
}
