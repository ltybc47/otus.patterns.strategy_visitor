﻿namespace StrategyPattern
{
    abstract class Shape
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Shape(int x, int y)
        {
            X = x;
            Y = y;
        }
        public abstract string Export(IVisitor visiter);

    }
}
