﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class Circle :  Shape
    {
        public Circle(int x, int y, int radius) : base(x, y)
        {
            Radius = radius;
        }

        public int Radius { get; }

        public override string Export(IVisitor visiter) => visiter.Export(this);
    }
}
