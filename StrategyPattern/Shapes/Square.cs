﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class Square : Shape
    {
        public Square(int x, int y, int side) : base(x, y)
        {
            Side = side;
        }

        public int Side { get; }

        public override string Export(IVisitor visiter)
        {
            return visiter.Export(this);
        }
    }
}
