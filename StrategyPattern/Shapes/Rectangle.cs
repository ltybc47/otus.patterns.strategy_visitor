﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class Rectangle : Shape
    {
        public int Width { get; }
        public int Height { get; }

        public Rectangle(int x, int y, int width, int height) : base(x, y)
        {
            Width = width;
            Height = height;
        }

        public override string Export(IVisitor visiter) => visiter.Export(this);
    }
}