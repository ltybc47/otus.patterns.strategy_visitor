﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class Context
    {
        private IExport export;

        public Context(IExport export)
        {
            this.export = export;
        }

        public string DoExport(Shape shape)
        {
            return export.Export(shape);
        }

        public string DoExport(Rectangle shape)
        {
            return export.Export(shape);
        }

        public string DoExport(Circle shape)
        {
            return export.Export(shape);
        }

        public string DoExport(Square shape)
        {
            return export.Export(shape);
        }
    }
}
