﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class ExportXml : IExport
    {
        public string Export(Shape shape)
        {
            return $@"<SHAPE>
<X>{shape.X}</X>
<Y>{shape.Y}</Y>
</SHAPE>";
        }

        public string Export(Circle circle)
        {
            return $@"<Circle>
<X>{circle.X}</X>
<Y>{circle.Y}</Y>
<Radius>{circle.Radius}</Radius>
</Circle>";
        }

        public string Export(Rectangle rectangle)
        {
            return $@"<Circle>
<X>{rectangle.X}</X>
<Y>{rectangle.Y}</Y>
<Height>{rectangle.Height}</Height>
<Width>{rectangle.Width}</Width>
</Circle>";
        }

        public string Export(Square square)
        {
            return $@"<Square>
<X>{square.X}</X>
<Y>{square.Y}</Y>
<Radius>{square.Side}</Radius>
</Square>";
        }
    }
}
