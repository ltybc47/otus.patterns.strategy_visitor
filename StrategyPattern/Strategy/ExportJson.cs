﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    class ExportJson : IExport
    {
        public string Export(Shape shape)
        {
            return $@"{{""X"":{shape.X}, ""Y"":{shape.Y}}}";
        }

        public string Export(Circle circle)
        {
            return
                $@"{{""X"":{circle.X},""Y"":{circle.Y},""Radius"": {circle.Radius}}}";
        }

        public string Export(Rectangle rectangle)
        {
            return $@"{{""X"":{rectangle.X}, ""Y"":{rectangle.Y},""Height"": {rectangle.Height}, ""Width"": {rectangle.Width}}}";
        }

        public string Export(Square square)
        {
            return
                $@"{{""X"":{square.X},""Y"":{square.Y},""Radius"": {square.Side}}}";
        }
    }
}
