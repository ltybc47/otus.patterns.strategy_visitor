﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    interface IExport
    {
        string Export(Shape shape);
        string Export(Circle circle);
        string Export(Rectangle rectangle);
        string Export(Square square);
    }
}
